﻿using UnityEngine;

public class MainArray : MonoBehaviour
{
    public static int[] arr = new int[100000];

    void Start()
    {
        Unsort();
    }

    public void ShowArray()
    {
        for (int i = 0; i < arr.Length; i++)
        {
            Debug.Log(arr[i]);
        }
    }
    public void Unsort()
    {
        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = Random.Range(0, 10000);
        }
    }
}
