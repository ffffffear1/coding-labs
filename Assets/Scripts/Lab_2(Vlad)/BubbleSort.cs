﻿using System;
using UnityEngine;

public class BubbleSort : MonoBehaviour
{
    DateTime StartTime;
    DateTime EndTime;
    
    public void BubbleSortStart()
    {
        StartTime = DateTime.Now;

        int z = 0;
        bool isSorted = true;

        while(isSorted == true)
        {
            isSorted = false;

            for (int i = 0; i < MainArray.arr.Length-1; i++)
            {
                if (MainArray.arr[i + 1] < MainArray.arr[i])
                {
                    z = MainArray.arr[i];
                    MainArray.arr[i] = MainArray.arr[i + 1];
                    MainArray.arr[i + 1] = z;
                    isSorted = true;
                }
            }
        }

        EndTime = DateTime.Now;
        TimeSpan time = EndTime - StartTime;

        Debug.Log(time);
    }
}
