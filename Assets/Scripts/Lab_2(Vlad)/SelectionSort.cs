﻿using System;
using UnityEngine;

public class SelectionSort : MonoBehaviour
{
    DateTime StartTime;
    DateTime EndTime;

    public void SelectionSortStart()
    {
        StartTime = DateTime.Now;

        int z = 0;

        for (int j = 0; j < MainArray.arr.Length; j++)
        {
            for (int i = 0; i < MainArray.arr.Length; i++)
            {
                if(MainArray.arr[j] < MainArray.arr[i])
                {
                    z = MainArray.arr[j];
                    MainArray.arr[j] = MainArray.arr[i];
                    MainArray.arr[i] = z;
                }
            }
        }

        EndTime = DateTime.Now;
        TimeSpan time = EndTime - StartTime;

        Debug.Log(time);
    }
}
