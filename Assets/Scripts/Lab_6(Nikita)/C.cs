﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class C : MonoBehaviour , IScalable , IMovable
{
    void Start()
    {
        Move(2, 4, 6);
        Scale(50);
    }

    public void Move(float x, float y, float z) => transform.Translate(new Vector3(x*2, y*2, z*2));

    public void Scale(float scale) => transform.localScale = new Vector3(1 - scale * 0.01f, 1 - scale * 0.01f, 1 - scale * 0.01f);
}
