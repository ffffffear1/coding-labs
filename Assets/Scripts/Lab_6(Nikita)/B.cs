﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class B : MonoBehaviour , IMovable, IRotatable
{
    void Start() => Move(2, 4, 6);

    void Update() => Rotate(3);

    public void Move(float x, float y, float z) => transform.Translate(new Vector3(-x, -y, -z));

    public void Rotate(float angle) => transform.Rotate(new Vector3(-angle, -angle, -angle));
}
