﻿public interface IScalable 
{
    void Scale(float scale);
}
