﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class A : MonoBehaviour , IMovable , IRotatable , IScalable
{
    void Start()
    {
        Move(2, 4, 6);
        Scale(50);
    }

    void Update() => Rotate(3);

    public void Move(float x, float y, float z) => transform.Translate(new Vector3(x, y, z));

    public void Rotate(float angle) => transform.Rotate(new Vector3(angle, angle, angle));

    public void Scale(float scale) => transform.localScale = new Vector3(1 + scale * 0.01f, 1 + scale * 0.01f, 1 + scale * 0.01f);
}
