﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class InterfaceController : MonoBehaviour
{
    public GameObject cube;

    List<IScalable> scalables = new List<IScalable>();
    List<IMovable> movables = new List<IMovable>();
    List<IRotatable> rotatables = new List<IRotatable>();

    void Start()
    {
        for (int i = 0; i < 15; i++)
        {
            GameObject newCube = Instantiate(cube, new Vector3(Random.Range(0, 10), Random.Range(0, 10), Random.Range(0, 10))
                , transform.rotation);
            int acbd = Random.Range(0, 4);
            
            if (acbd == 0)
            {
                newCube.name = "Cube_A"; 
                newCube.AddComponent<A>(); 
                //movables.Add();
            }
            else if (acbd == 1)
            {
                newCube.name = "Cube_B"; 
                newCube.AddComponent<B>();
            }
            else if (acbd == 2)
            {
                newCube.name = "Cube_C"; 
                newCube.AddComponent<C>();
            }
            else
            {
                newCube.name = "Cube_D"; 
                newCube.AddComponent<D>();
            }
        }
    }

    private void Update()
    {
        
    }
}
