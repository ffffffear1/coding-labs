﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class D : MonoBehaviour , IRotatable , IScalable
{
    void Start() => Scale(50);

    void Update() => Rotate(3);

    public void Rotate(float angle) => transform.Rotate(new Vector3(angle*2, angle*2, angle*2)); 

    public void Scale(float scale) => transform.localScale = new Vector3(scale * 0.01f, scale * 0.01f, scale * 0.01f);

}
