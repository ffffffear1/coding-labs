﻿using Assets.SimpleLocalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;

public class TableImporter: MonoBehaviour
{
    [SerializeField]
    private string _tableId;

    [SerializeField]
    private string _pageId;

    private const string _urlPattern = "https://docs.google.com/spreadsheets/d/{0}/export?format=csv&gid={1}";

    public void GetValues(Action<List<string[]>> callback)
    {
        var url = string.Format(_urlPattern, _tableId, _pageId);

        Debug.Log(url);

        Downloader.Download(url, www =>
        {
            if (www.error == null)
            {
                System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
                string str = enc.GetString(www.bytes);
                callback.Invoke(Read(str));
                Debug.Log("Import <color=green>sucsess!</color>");
            }
            else
            {
                Debug.Log("Import <color=red>failed!</color>");
            }
            DestroyImmediate(Downloader.Instance.gameObject);
        });
    }

    private List<string[]> Read(string text)
    {
        Regex CSVParser = new Regex(",(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))");

        Regex r = new Regex(@"(?m)^[^""\r\n]*(?:(?:""[^""]*"")+[^""\r\n]*)*");
        MatchCollection m = r.Matches(text);
       
        List<string[]> values = new List<string[]>();


        string[] lines =  m.Cast<Match>().Select(ma => ma.Value).ToArray();
        foreach (string line in lines)
        {
            String[] Fields = CSVParser.Split(line);
            int i = 0;
            foreach (string s in Fields)
            {
                if (s.StartsWith("\"")&& s.EndsWith("\""))
                {
                    Fields[i] = s.Substring(1, s.Length-2);
                    Fields[i] = RemoveDoubleQuotes(Fields[i]);
                }

                i++;
            }
            values.Add(Fields);
        }
        return values;
    }

    private static string RemoveDoubleQuotes(string text)
    {
        Regex r = new Regex("(\")\\1+");
        MatchCollection m = r.Matches(text);
        return Regex.Replace(text, "(\")\\1+", "\"");
    }
    private static string ReplaceMarkers(string text)
    {
        return text.Replace("[Newline]", "\n");
    }
}
