﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

[Flags]
enum AcademicSubjects
{
    Math,
    Phys,
    OOP,
    Ver,
    OBJ,
    History
}

public class Student
{
    public string Fistname;
    public string Lastname;
    public int Course;
    Dictionary<AcademicSubjects, int> c = new Dictionary<AcademicSubjects, int>();
    public float scholarship;
    public string middleScore {
        get { return c.Values.Average().ToString("f1"); }
    }

    public Student(string fistname, string lastname, int course, List<string> academicSubjects, float scholarship)
    {
        Fistname = fistname;
        Lastname = lastname;
        Course = course;
        this.scholarship = scholarship;

        for (int i = 0; i < 6; i++)
        {
            c.Add((AcademicSubjects)i, int.Parse(academicSubjects[i]));
        }
    }
}

public class Lab : MonoBehaviour
{
    private List<Student> students = new List<Student>();
    private List<string[]> values = new List<string[]>();

    [ContextMenu("Download")]
    void Download()
    {
        GetComponent<TableImporter>().GetValues(Show);
        AcademicSubjectsShow(values);
    }

    [ContextMenu("Best Students")]      // доделать вывод!!
    void BestStudents()
    {
        students.OrderBy(x => x.middleScore);
        GetComponent<TableImporter>().GetValues(Show);
    }

    void AcademicSubjectsShow(List<string[]> values)
    {
        int i = 0;

        Debug.Log(values.Count);
        foreach (string[] line in values)
        {
            foreach (string s in line)
            {
                i++;
                if (i >= 2)
                {
                    Debug.Log(s);
                }
            }
            break;
        }
    }

    void Show(List<string[]> values)
    {
        Debug.Log(values.Count);
        foreach (string[] line in values)
        {
            if (line == values[0])
                continue;

            Student student = new Student(line[0], line[1], int.Parse(line[2]), line.ToList().GetRange(4, 6), float.Parse(line[3]));

            students.Add(student);

            Debug.Log("Фамилия: " + student.Lastname + " Имя: "+ student.Fistname + " Курс: " + student.Course + " Средний бал: " + student.middleScore + " Степендия: " + student.scholarship);

        }
    }
}
