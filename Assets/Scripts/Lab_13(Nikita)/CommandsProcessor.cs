﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class CommandsProcessor : MonoBehaviour
{
    public Sprite[] image;
    
    private List<ICommand> commands = new List<ICommand>();
    
    private List<Color> colors = new List<Color>();

    private SpriteRenderer _spriteRenderer;
    private Transform _transform;

    private void Start()
    {
        #region Bынужденная ситуация
        colors.Add(Color.cyan);
        colors.Add(Color.magenta);
        colors.Add(Color.yellow);
        colors.Add(Color.blue);
        colors.Add(Color.gray);
        colors.Add(Color.red);
        colors.Add(Color.black);
        #endregion 
        
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _transform = GetComponent<Transform>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            var newCommand = new Move(_transform, new Vector3(0,1));
            newCommand.Execute();
            commands.Add(newCommand);
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            var newCommand = new Move(_transform, new Vector3(0,-1));
            newCommand.Execute();
            commands.Add(newCommand);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            var newCommand = new Move(_transform, new Vector3(-1,0));
            newCommand.Execute();
            commands.Add(newCommand);
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            var newCommand = new Move(_transform, new Vector3(1,0));
            newCommand.Execute();
            commands.Add(newCommand);
        }
            
        if(Input.GetKeyDown(KeyCode.Alpha1)) 
        {
            var newCommand = new ChangeSprite(_spriteRenderer, image[0]);
            newCommand.Execute();
            commands.Add(newCommand);
        }
        if(Input.GetKeyDown(KeyCode.Alpha2)) 
        {
            var newCommand = new ChangeSprite(_spriteRenderer, image[1]);
            newCommand.Execute();
            commands.Add(newCommand);
        }
        if(Input.GetKeyDown(KeyCode.Alpha3)) 
        {
            var newCommand = new ChangeSprite(_spriteRenderer, image[2]);
            newCommand.Execute();
            commands.Add(newCommand);
        }
        if(Input.GetKeyDown(KeyCode.X)) 
        {
            var newCommand = new ChangeColor(_spriteRenderer, 
                colors[Random.Range(0, colors.Count-1)]);
            newCommand.Execute();
            commands.Add(newCommand);
        }
        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            commands.Last().Undo();
        }
    }
}
