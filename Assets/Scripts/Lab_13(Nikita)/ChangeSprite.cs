﻿using UnityEngine;

public class ChangeSprite : ICommand
{
    private Sprite newSprite;
    private Sprite oldSprite;
    private SpriteRenderer spriteRenderer;
    
    public ChangeSprite(SpriteRenderer _spriteRenderer, Sprite _sprite)
    {
        newSprite = _sprite;
        spriteRenderer = _spriteRenderer;
    }
    
    public void Execute()
    {
        oldSprite = spriteRenderer.sprite;
        spriteRenderer.sprite = newSprite;
    }

    public void Undo()
    {
        spriteRenderer.sprite = oldSprite;
    }
}
