﻿using UnityEngine;

public class Move : ICommand
{
    private Transform transform;
    private Vector3 newPos;
    
    public Move(Transform _transform, Vector3 _newPos)
    {
        newPos = _newPos;
        transform = _transform;
    }
    
    public void Execute()
    {
        transform.position += newPos;
    }

    public void Undo()
    {
        transform.position -= newPos;
    }
}
