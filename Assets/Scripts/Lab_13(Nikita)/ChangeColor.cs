﻿using UnityEngine;

public class ChangeColor : ICommand
{
    private Color newColor;
    private Color oldColor;
    private SpriteRenderer spriteRenderer;
    
    public ChangeColor(SpriteRenderer _spriteRenderer, Color _color)
    {
        newColor = _color;
        spriteRenderer = _spriteRenderer;
    }

    public void Execute()
    {
        oldColor = spriteRenderer.color;
        spriteRenderer.color = newColor;
    }

    public void Undo()
    {
        spriteRenderer.color = oldColor;
    }
}
