﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DiceGameLab
{
    public class DiceView : MonoBehaviour
    {
        private Dice dice;
        
        public void Init(Dice _dice)
        {
            dice = _dice;
            dice.OnDiceChanged += ChangedCube;
        }
        
        private void ChangedCube()
        {
            transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>().text = dice.Value.ToString();
            transform.GetChild(0).gameObject.SetActive(dice.IsBlocked);
        } 
        
        public void Click() => dice.IsBlocked = !dice.IsBlocked;
    }
}