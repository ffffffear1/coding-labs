﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Debug = System.Diagnostics.Debug;
using Random = UnityEngine.Random;

namespace DiceGameLab
{
    public class DiceGame : MonoBehaviour
    {
        public GameObject cude;
        public GameObject combination;
        public Transform combinationSpawn;
        
        private List<Dice> dices = new List<Dice>();
        private List<Combination> combinations = new List<Combination>();
        private bool canSave = true;
        private int endGame = 0;
        private float endScore = 0;
        
        private void Start()
        {
            for (int i = 0; i < 6; i++)
            {
                dices.Add(new Dice());
                var newCube = Instantiate(cude, transform);
                newCube.GetComponent<DiceView>().Init(dices[i]);

                combinations.Add(new Combination(Random.Range(1,30)));
                var newCombination = Instantiate(combination, combinationSpawn);
                newCombination.GetComponent<CombinationView>().Init(combinations[i]);
                newCombination.transform.GetChild(0).GetChild(1).GetComponent<TextMeshProUGUI>().text = i + 1 +"";
            }
        }

        public void Roll()
        {
            if (canSave)
            {
                foreach (var dice in dices)
                {
                    if (!dice.IsBlocked) dice.Value = Random.Range(1, 6);
                }
                canSave = false;
            }
        }

        public void FillCombination(Combination _combination)
        {
            _combination.IsFilled = true;
            foreach (var dice in dices)
            {
                _combination.Score += dice.Value;
            }
            endScore += _combination.Score;
            endGame++;
            if (endGame >= 6) print($"Ваш счет {endScore}");
        }

        public void Restart() => SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
        public bool CanSave
        {
            get => canSave;
            set => canSave = value;
        }
    }
}