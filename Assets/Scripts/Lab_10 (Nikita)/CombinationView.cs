﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DiceGameLab
{
    public class CombinationView : MonoBehaviour
    {
        private Combination combination;

        public void Init(Combination _combination)
        {
            combination = _combination;
            combination.OnCombinationChanged += ChangeCombination;
        }

        private void ChangeCombination()
        {
            transform.GetChild(0).GetChild(0).GetComponent<TextMeshProUGUI>().text = combination.Score.ToString();
            transform.GetChild(0).GetComponent<Button>().enabled = false;
            transform.GetChild(0).GetComponent<Image>().color = Color.gray;
        }

        public void Click()
        {
            var diceGame = FindObjectOfType<DiceGame>();
            if (!diceGame.CanSave)
            {
                diceGame.FillCombination(combination);
                diceGame.CanSave = true;
            }
        } 
    }
}
