﻿using System;

namespace DiceGameLab
{
    public class Combination
    {
        public Action OnCombinationChanged;

        private bool isFilled;
        private int value;
        private int score;

        public Combination(int _value)
        {
            value = _value;
        }

        public bool IsFilled
        {
            get => isFilled;
            set
            {
                isFilled = value;
                OnCombinationChanged();
            }
        }

        public int Value
        {
            get => value;
            set
            {
                this.value = value;
                OnCombinationChanged();
            }
        }

        public int Score
        {
            get => score;
            set
            {
                score = value;
                OnCombinationChanged();
            }
        }
    }
}