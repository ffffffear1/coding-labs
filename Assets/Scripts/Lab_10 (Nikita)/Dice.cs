﻿using System;

namespace DiceGameLab
{
    public class Dice
    {
        public Action OnDiceChanged;

        private int value;
        private bool isBlocked;

        public int Value
        {
            get => value;
            set
            {
                this.value = value;
                OnDiceChanged();
            }
        }

        public bool IsBlocked
        {
            get => isBlocked;
            set
            {
                isBlocked = value;
                OnDiceChanged();
            }
        }
    }
}
