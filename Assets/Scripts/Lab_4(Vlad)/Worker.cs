﻿
namespace Workers
{
    public abstract class Worker
    {
        public string name;
        public float salary;
        public float monthlySalary;

        public abstract float Salary();
    }
}
