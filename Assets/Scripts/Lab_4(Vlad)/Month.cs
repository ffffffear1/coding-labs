﻿using UnityEngine;

namespace Workers
{
    public class Month : Worker
    {
        public override float Salary()
        {
            monthlySalary = salary;
            return monthlySalary;
        }
    }
}