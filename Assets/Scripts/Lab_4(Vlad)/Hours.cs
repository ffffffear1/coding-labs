﻿using UnityEngine;

namespace Workers
{
    public class Hours : Worker
    {
        public override float Salary()
        {
            monthlySalary = 20.8f * 8 * salary;
            return monthlySalary;
        }
    }
}