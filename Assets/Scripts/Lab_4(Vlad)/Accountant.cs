﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Workers
{
    public class Accountant : MonoBehaviour
    {
        List<Worker> workers = new List<Worker>();

        void Start()
        {
            workers.Add(new Hours() { name = "Gosha", salary = 450 });
            workers.Add(new Hours() { name = "Tolya", salary = 903 });
            workers.Add(new Hours() { name = "Gera", salary = 250 });
            workers.Add(new Hours() { name = "Sasha", salary = 150 });

            workers.Add(new Month() { name = "Masha", salary = 45000 });
            workers.Add(new Month() { name = "Kostya", salary = 90001 });
            workers.Add(new Month() { name = "Lena", salary = 25000 });
            workers.Add(new Month() { name = "Max", salary = 1213950 });

            foreach(Worker worker in workers)
            {
                // worker.monthlySalary = 
                Debug.Log(worker.name + " " + worker.monthlySalary);
            }

            workers = workers.OrderByDescending(a => a.monthlySalary).ToList();

            foreach (Worker worker in workers)
            {
                Debug.Log(worker.name + " " + worker.monthlySalary);
            }
        }
    }
}
