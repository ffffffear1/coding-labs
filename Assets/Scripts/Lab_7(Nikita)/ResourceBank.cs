﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceBank : MonoBehaviour
{
    public enum GameResource
    {
        Humans, Food, Wood, Stone, Gold
    }

    private ObservableInt value = new ObservableInt();

    void Start()
    {
        value.Value++;
    }

    void Update()
    {
        
    }
    public void ChangeResource(GameResource r, int v)
    {
        
    }

    public void GetResource(GameResource r)
    {

    }
}