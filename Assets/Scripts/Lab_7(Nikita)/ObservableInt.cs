﻿using UnityEngine;

public class ObservableInt : MonoBehaviour
{
    private int value;

    //public Action<int> onValueChanged = (v) => { }; onValueChanged(this.value); 

    public int Value
    {
        get { return value; }
        set { this.value = value; OnValueChanged(value); }
    }

    public static void OnValueChanged(int value)
    {
        Debug.Log(value);
    }
}
