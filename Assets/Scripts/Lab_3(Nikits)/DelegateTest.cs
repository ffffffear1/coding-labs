﻿using UnityEngine;

public class DelegateTest : MonoBehaviour
{
    public delegate void DelegateDebug();

    void Start()
    {
        DelegateDebug del_deb;
        DelegateDebug del_deb_1;
        DelegateDebug del_deb_2;

        del_deb = DelegateText;
        del_deb();
        del_deb_1 = DelegateTextCool;
        del_deb_1();
        del_deb_2 = del_deb + del_deb_1;
        del_deb_2();
    }

    private void DelegateText()
    {
        Debug.Log("Делегаты");
    }
    private void DelegateTextCool()
    {
        Debug.Log("Это круто");
    }
}
