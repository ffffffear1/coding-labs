﻿using UnityEngine;

public class DelegateDifference : MonoBehaviour
{
    private delegate float DelegateDifferenceTest(float a, float b);

    void Start()
    {
        DelegateDifferenceTest delegateDifferenceTest = (a, b) => a / b;
        Debug.Log(delegateDifferenceTest(8.4f, 2.2f));
    }
}
