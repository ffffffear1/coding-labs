﻿using UnityEngine;

public class DelegateAdd : MonoBehaviour
{
    private delegate float DelegateAdder(float a, float b, float c);

    void Start() 
    {
        DelegateAdder @delegate = (a, b, c) => a + b + c;
        Debug.Log(@delegate(5.3f, 3.4f, 2.3f));
    }
}
