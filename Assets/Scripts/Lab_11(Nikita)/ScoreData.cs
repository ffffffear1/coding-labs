﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class ScoreData : MonoBehaviour
{
    public TMP_InputField InputField;
    public TextMeshProUGUI text;
    
    private List<PlayerScore> _scores = new List<PlayerScore>();

    public void ClearAll()
    {
        _scores.Clear();
        text.text = null;
    }
    
    public void AddPlayer()
    {
        if (InputField.text != "")
        {
            var newPlayer = new PlayerScore(InputField.text, Random.Range(0, 10000));
            _scores.Add(newPlayer);
            text.text += $"{newPlayer.Nickname} {newPlayer.ScoreValue} \r\n";
        }
    }
    
    public void OverScore()
    {
        IEnumerable<PlayerScore> players = _scores.Where(score => score.ScoreValue > 5000);
        text.text += "Players with a score greater than 5000 \r\n";
        Output(players);
    }
    
    public void OverNickname()
    {
        IEnumerable<PlayerScore> players = _scores.Where(nickname => nickname.Nickname.Length > 5);
        text.text += "A list of players with a nickname longer than 5 \r\n";
        Output(players);
    }
    
    public void SortScore()
    {
        IEnumerable<PlayerScore> players = _scores.OrderBy(score => score.ScoreValue);
        text.text += "Score sorted list \r\n";
        Output(players);
    }
    
    public void CheckScoreOver()
    {
        IEnumerable<PlayerScore> players = _scores.Where(score => score.ScoreValue > 7500);
        text.text += "There is a player in the list with an account greater than 7500 \r\n";
        if (players.Any()) text.text += "true \r\n";
        else text.text += "false \r\n";
    }

    public void MaxScorePlayer()
    {
        IEnumerable<PlayerScore> player = _scores.OrderByDescending(score => score.ScoreValue);
        text.text += "Player with the highest score \r\n";
        text.text += $"{player.First().Nickname} \r\n";
    }

    public void TopTenPlayers()
    {
        IEnumerable<PlayerScore> players = _scores.OrderByDescending(score => score.ScoreValue);
        text.text += "Top 10 players EUR \r\n";
        if (players.Count() >= 10)
        {
            var tenPlayer = players.Take(10);
            Output(tenPlayer);
        } else Output(players);
    }

    public void FirstOverTwoThousand()
    {
        IEnumerable<PlayerScore> players = _scores.Where(score => score.ScoreValue > 2000);
        text.text += "First over two thousand player \r\n";
        text.text += $"{players.First().Nickname} {players.First().ScoreValue} \r\n";
    }
    
    private void Output(IEnumerable<PlayerScore> players)
    {
        foreach (var player in players)
        {
            text.text += $"{player.Nickname} {player.ScoreValue} \r\n";
        }
    }
}
