﻿public class PlayerScore
{
    private string nickname;
    private float scoreValue;

    public PlayerScore(string _nickname, float _scoreValue)
    {
        nickname = _nickname;
        scoreValue = _scoreValue;
    }

    public string Nickname
    {
        get => nickname;
        set => nickname = value;
    }

    public float ScoreValue
    {
        get => scoreValue;
        set => scoreValue = value;
    }
}
