﻿public class ChangeSpeed : GameCommand
{
    public override void Process()
    {
        PlayerInfo.instance.playerSpeed += value;
        base.Process();
    }

    public override void Undo()
    {
        PlayerInfo.instance.playerSpeed -= value;
        base.Undo();
    } 
}
