﻿public class ChangeScale : GameCommand
{
    public override void Process()
    {
        PlayerInfo.instance.playerSize.Value += value;
        base.Process();
    }

    public override void Undo()
    {
        PlayerInfo.instance.playerSize.Value -= value;
        base.Undo();
    } 
}
