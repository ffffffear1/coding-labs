﻿using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    private void Start()
    {
        PlayerInfo.instance.playerSize.OnValueChanged += ChangeScale;
    }

    private void Update()
    {
        var X = Input.GetAxis("Horizontal") * PlayerInfo.instance.playerSpeed * Time.deltaTime;
        var Y = Input.GetAxis("Vertical") * PlayerInfo.instance.playerSpeed * Time.deltaTime;
        
        transform.Translate(new Vector2(X, Y));
    }

    private void ChangeScale(float s) => transform.localScale = new Vector2(s,s);
    
}
