﻿using System;
using UnityEngine;

public class Bonus : MonoBehaviour
{
    public float duration;
    public CommandType cType;
    public float value;
    
    public enum CommandType
    {
        ChangeSpeed, 
        ChangeScale
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInfo.instance.CollectBonus(GetComponent<Bonus>());
            Destroy(gameObject);
        }
    }
}
