﻿using System;
using UnityEngine;

public class GameCommand : MonoBehaviour
{
    public MinMaxValue timer;
    public float value;

    public Action<GameCommand> OnCommandUndo = command => { };

    public void Init(float duration, float _value)
    {
        value = _value;
        timer = new MinMaxValue(duration, 0, duration);
    }
    
    public virtual void Process()
    {
        
    } 
    
    public virtual void Undo()
    {
        OnCommandUndo(this);
    }

    public void Tick()
    {
        timer._Value -= Time.deltaTime;
        if(timer._Value <= 0) Undo();
    }
}
