﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    public float playerSpeed;
    public MinMaxFloat playerSize = new MinMaxFloat(5, 5,10);
    public List<GameCommand> commands = new List<GameCommand>();
    
    public static PlayerInfo instance;
    
    void Awake()
    {
        if (instance == null) instance = this;
        else if(instance == this) Destroy(gameObject);
    }

    private void Update()
    {
        for (int i = 0; i < commands.Count; i++)
        {
            commands[i].Tick();
        }
    }

    public void CollectBonus(Bonus bonus)
    {
        var newCommand = (GameCommand)Activator.CreateInstance(Type.GetType(bonus.cType.ToString()));
        newCommand.Init(bonus.duration, bonus.value);
        newCommand.Process();
        newCommand.OnCommandUndo += DeleteCommand;
        commands.Add(newCommand);
    }

    private void DeleteCommand(GameCommand obj) => commands.Remove(obj);
    
}
