﻿using System;
using UnityEngine;

public class MinMaxFloat
{
    private float _value, _min, _max;

    public float Max
    {
        get
        {
            return _max;
        }
    }

    public Action<float> OnValueChanged = (v) => { };

    public float Value
    {
        get
        {
            return _value;
        }
        set
        {
            _value = Mathf.Clamp(value, _min, _max);
            OnValueChanged(_value);
        }
    }

    public MinMaxFloat(float v, float min, float max)
    {
        _value = v;
        _min = min;
        _max = max;
    }
}
