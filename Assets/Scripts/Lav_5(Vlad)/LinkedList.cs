﻿using UnityEngine;

namespace Linked
{
    public class LinkedList : MonoBehaviour
    {
        static Node tail = null;
        static Node head = null;

        public static void addList(int value)
        {
            if (tail == null)
                tail = new Node(null, value);
            else if(head == null)
            {
                tail.next = new Node(null, value);
                head = tail.next;
            }
            else {
                head.next = new Node(null, value);
                head = head.next;
            }
        }

        public static void printList()
        {
            Node lastNode = tail;

            if (lastNode != null)
            {
                while (lastNode != null)
                {
                    Debug.Log(lastNode.value);
                    lastNode = lastNode.next;
                }
            }
            else { Debug.Log("Список пуст"); }
        }

        public static void clear()
        {
            Node lastNode = tail;

            if (lastNode != null)
            {
                while (tail != null)
                {
                    lastNode = lastNode.next;
                    tail = null;
                    tail = lastNode;
                }
            } else { Debug.Log("Список уже пуст"); }
        }

        public static void count()
        {
            Node lastNode = tail;
            int count = 0;

            while (lastNode != null)
            {
                count++;
                lastNode = lastNode.next;
            }
            Debug.Log(count);
        }
    }
}