﻿using UnityEngine;

namespace Linked
{
    public class App : MonoBehaviour
    {
        public void Start()
        {
            LinkedList.printList();
            LinkedList.clear();
            LinkedList.count();

            LinkedList.addList(3);
            LinkedList.addList(5);
            LinkedList.addList(65);
            LinkedList.addList(9);

            LinkedList.printList();
            LinkedList.count();
            LinkedList.clear();
            LinkedList.count();
        }
    }
}
