﻿using UnityEngine;

public class MinMaxValue : ObservableFloat
{
    private float minValue , maxValue;
    
    public MinMaxValue(float furstValue, float min, float max) : base(furstValue)
    {
        minValue = min;
        maxValue  = max;
        _Value = furstValue;
    }

    public float _Value
    {
        get => Value;
        set => Value = Mathf.Clamp(value, minValue, maxValue);
    }
}
