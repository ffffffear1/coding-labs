﻿using UnityEditor.SceneManagement;
using UnityEngine;

public class FloatController : MonoBehaviour 
{
    public MinMaxValue minMaxValue;
    
    private ValueVisual valueVisual;
    
    void Start()
    {
        valueVisual = FindObjectOfType<ValueVisual>();
        minMaxValue = new MinMaxValue(2, 5, 20);
        valueVisual.Init(minMaxValue);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) minMaxValue._Value += 1;
    }
}
