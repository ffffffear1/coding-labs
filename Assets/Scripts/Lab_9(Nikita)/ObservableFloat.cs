﻿using System;

public class ObservableFloat
{
    private float _value;

    public Action<float> OnValueChanged = (f) => { };

    public ObservableFloat(float _Value)
    {
        Value = _Value;
    }
    
    public float Value
    {
        get => _value;
        set
        {
            _value = value;
            OnValueChanged(_value);
        }
    }
}
