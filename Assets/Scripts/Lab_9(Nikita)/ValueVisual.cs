﻿using UnityEngine;
using UnityEngine.UI;

public class ValueVisual : MonoBehaviour
{
    public Text showValue;
    
    public void Init(ObservableFloat f) => f.OnValueChanged = UpdateView;
    private void UpdateView(float newValue) => showValue.text = newValue.ToString();
}
