﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ActionQueue : MonoBehaviour
{
    public Queue<Action> actions = new Queue<Action>(); 
    
    void Start() => StartCoroutine(startActionList());

    IEnumerator startActionList()
    {
        while (true)
        {
            if (actions.Count > 0) actions.Dequeue().Invoke();
            yield return new WaitForSeconds(1f);
        }
    }

    public void AddAction(Action move) => actions.Enqueue(move);
}
