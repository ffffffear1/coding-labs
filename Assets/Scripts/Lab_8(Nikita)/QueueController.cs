﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class QueueController : MonoBehaviour
{
    private ActionQueue _actionQueue;
    private Vector3 curPos;

    private void Start() => _actionQueue = FindObjectOfType<ActionQueue>();

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W)) _actionQueue.AddAction(MoveF);
        if (Input.GetKeyDown(KeyCode.S)) _actionQueue.AddAction(MoveB);
        if (Input.GetKeyDown(KeyCode.D)) _actionQueue.AddAction(MoveL);
        if (Input.GetKeyDown(KeyCode.A)) _actionQueue.AddAction(MoveR);
    }
    
    private void MoveF() => transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 1);
    private void MoveB() =>transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 1);
    private void MoveL() => transform.position = new Vector3(transform.position.x + 1, transform.position.y, transform.position.z); 
    private void MoveR() => transform.position = new Vector3(transform.position.x - 1, transform.position.y, transform.position.z);
    

    // private void Move(Vector3 dir)
    // {
    //     transform.position = dir * speed * Time.deltaTime;
    // }
}
